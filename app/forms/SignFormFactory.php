<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;


class SignFormFactory extends Nette\Object
{
	/** @var User */
	private $user;


	public function __construct(User $user)
	{
		$this->user = $user;
	}


	/**
	 * @return Form
	 */
	public function create()
	{
		$form = new Form;
		$form->getElementPrototype()->addAttributes(array('class'=>'form-signin'));
		$form->addText('username', NULL)
				->setRequired('Please enter your username.')
				->setAttribute('class', 'form-control')
				->setAttribute('placeholder', 'Jméno');

		$form->addPassword('password',NULL)
				->setRequired('Please enter your password.')
				->setAttribute('class', 'form-control')
				->setAttribute('placeholder', 'Heslo');

		$form->addCheckbox('remember', 'Zapamatovat si mě');

		$form->addSubmit('send', 'Přihlásit')
				->setAttribute('class', ' btn btn-lg btn-primary btn-block');

		$form->onSuccess[] = array($this, 'formSucceeded');
		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{
		if ($values->remember) {
			$this->user->setExpiration('14 days', FALSE);
		} else {
			$this->user->setExpiration('20 minutes', TRUE);
		}

		try {
			$this->user->login($values->username, $values->password);
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError($e->getMessage());
		}
	}

}
