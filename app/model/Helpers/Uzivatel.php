<?php


namespace App\Model\Helpers;

use Nette\Database\Context;


class UzivatelHelper
{
	/** @var Context */
	private static $database;

	private static $table = 'users';

	public function __construct(Context $database) {
		self::$database = $database;
	}

	public function getAll($order = 'ASC', $limit = 2147483647, $offset = 0){
		$raw = self::$database->table(self::$table)->select('*')->order('id ' . $order)->limit($limit, $offset);

		$data = $raw->fetchAll();

		$ret = array();
		foreach($data as $d){
			$ret[] = new \App\Model\Entities\Uzivatel($d);
		}
		return $ret;
	}

	public function getById($id = null){
		if($id == null){
			return null;
		}
		$data = self::$database->table(self::$table)->select('*')->where('id', $id)->fetch();
		$ret =  new \App\Model\Entities\Uzivatel($data);
		return $ret;
	}

	public function save(\App\Model\Entities\Uzivatel $data){
		if($data->getId() != null){ //editace
			$ret = self::$database->table(self::$table)->where('id', $data->getId())->update($data->toArray());
		}else{ //vlozeni noveho zaznamu
			$ret = self::$database->table(self::$table)->insert($data->toArray());
		}

		return new \App\Model\Entities\Uzivatel($ret);
	}

	public function delete($id){
		self::$database->table(self::$table)->where('id', $id)->delete();
	}

	public function count(){
		return count(self::$database->table(self::$table)->select('id')->fetchAll());
	}
}