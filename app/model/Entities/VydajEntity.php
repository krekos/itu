<?php
/**
 * @author Radim Křek
 * Date: 15.12.2015
 */

namespace App\Model\Entities;


class Vydaj extends BaseEntity{
	/** @var int */
	public $id;

	/** @var int */
	public $book_id;

	/** @var string */
	public $title;

	/** @var int */
	public $price;

	/**
	 * @return int
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return VydajEntity
	 */
	public function setId($id){
		$this->id = $id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getBook_id(){
		return $this->book_id;
	}

	/**
	 * @param int $book_id
	 * @return VydajEntity
	 */
	public function setBook_id($book_id){
		$this->book_id = $book_id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle(){
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return VydajEntity
	 */
	public function setTitle($title){
		$this->title = $title;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getPrice(){
		return $this->price;
	}

	/**
	 * @param int $price
	 * @return VydajEntity
	 */
	public function setPrice($price){
		$this->price = $price;
		return $this;
	}

	public function toArray(array $notIncluded = array()){
		return parent::toArray($notIncluded);
	}
}