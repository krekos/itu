<?php

namespace App\Model\Entities;


class Book extends BaseEntity{

	/** @var int */
	protected $id;

	/** @var \DateTime */
	protected $datum;

	/** @var string */
	protected $start;

	/** @var string */
	protected $cil;

	/** @var int */
	protected $typ;

	/** @var int */
	protected $id_vozidla;

	/** @var VozidloEntity */
	protected $vozidlo;

	/** @var int */
	protected $stav;

	/** @var int */
	protected $vzdalenost;

	/** @var bool */
	protected $done = true;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return \DateTime
	 */
	public function getDatum()
	{
		return $this->datum;
	}

	/**
	 * @param \DateTime $datum
	 */
	public function setDatum($datum)
	{
		$this->datum = $datum;
	}

	/**
	 * @return string
	 */
	public function getStart()
	{
		return $this->start;
	}

	/**
	 * @param string $start
	 */
	public function setStart($start)
	{
		$this->start = $start;
	}

	/**
	 * @return string
	 */
	public function getCil()
	{
		return $this->cil;
	}

	/**
	 * @param string $cil
	 */
	public function setCil($cil)
	{
		$this->cil = $cil;
	}

	/**
	 * @return int
	 */
	public function getTyp()
	{
		return $this->typ;
	}

	/**
	 * @param int $typ
	 */
	public function setTyp($typ)
	{
		$this->typ = $typ;
	}

	/**
	 * @return int
	 */
	public function getId_vozidla()
	{
		return $this->id_vozidla;
	}

	/**
	 * @param int $id_vozidla
	 * @return Book
	 */
	public function setId_vozidla($id_vozidla)
	{
		$this->id_vozidla = $id_vozidla;
		return $this;
	}

	/**
	 * @return VozidloEntity
	 */
	public function getVozidlo()
	{
		return $this->vozidlo;
	}

	/**
	 * @param VozidloEntity $vozidlo
	 * @return Book
	 */
	public function setVozidlo($vozidlo)
	{
		$this->vozidlo = $vozidlo;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getStav()
	{
		return $this->stav;
	}

	/**
	 * @param int $stav
	 */
	public function setStav($stav)
	{
		$this->stav = $stav;
	}

	/**
	 * @return int
	 */
	public function getVzdalenost()
	{
		return $this->vzdalenost;
	}

	/**
	 * @param int $vzdalenost
	 */
	public function setVzdalenost($vzdalenost)
	{
		$this->vzdalenost = $vzdalenost;
	}

	/**
	 * @return boolean
	 */
	public function isDone(){
		return $this->done;
	}

	/**
	 * @param boolean $editable
	 * @return Book
	 */
	public function setDone($done){
		$this->done = $done;
		return $this;
	}



	public function toArray(array $notIncluded = array()){
		$notIncluded[] = 'vozidlo';
		return parent::toArray($notIncluded);
	}

	public function getTypeAsString(){
		switch($this->getTyp()){
			case 0:
				return 'Soukromá';
			case 1:
			default:
				return 'Služební';
		}
	}

	public function getStateAsString(){
		switch($this->getStav()){
			case 0:
			default:
				return 'Zpracovává se';
			case 1:
				return 'Proplaceno';
		}
	}

}