<?php

namespace App\Presenters;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;
use Nette\Utils\Paginator;


class UzivatelPresenter extends BasePresenter {

	public function startup()
	{
		if(!$this->user->isLoggedIn()){
			$backlink = $this->storeRequest();
			$this->forward('Sign:default', array('backlink' => $backlink));
		}
		parent::startup();
	}

	public function actionEdit($id = null){
		if($id) {
			$form = $this->getComponent('uzivatelForm');
			$data = $this->uzivatelHelper->getById($id);
			$data->setPassword(null);
			$form->setDefaults($data->toArray());
		}
	}

	public function renderEdit($id){
		$this->setView("add");
		$this->template->id = $id;
	}

	public function renderAdd(){
		$this->template->id = null;
	}

	public function renderDefault($page = 1, $q = null){
		$paginator = new Paginator();

		if($q){
			$data = $this->uzivatelHelper->search($q);
			$count = count($data);
		}else{
			$count = $this->uzivatelHelper->count();
		}

		$paginator->setItemCount($count);
		$paginator->setItemsPerPage(30);
		$paginator->setPage($page);

		if($q){
			$this->template->uzivatele = $data;
		}else{
			$this->template->uzivatele = $this->uzivatelHelper->getAll('ASC', $paginator->getLength(), $paginator->getOffset());
		}
		$this->template->paginator = $paginator;
		$this->redrawControl();
	}

	public function createComponentUzivatelForm(){
		$id = null;

		$req = $this->getRequest()->getParameters();
		if(array_key_exists('id', $req) && $req['id']!=null){
			$id = $req['id'];
		}

		$form = new Form();
		$form->addText('username', 'Login *')
			->setAttribute('class', 'form-control')
			->setRequired("Vyplňte login!");
		$pass = $form->addPassword('password', 'Heslo *')
			->setAttribute('class', 'form-control');
		if($id == null){
			$pass->setRequired("Vyplňte heslo!");
		}
		$pass2 = $form->addPassword('confirm_password', "Znovu heslo *")
			->setAttribute('class', 'form-control')
			->addConditionOn($form["password"], Form::FILLED)
			->addRule(Form::EQUAL, "Hesla se musí shodovat !", $form["password"]);
		if($id == null){
			$pass2->addRule(Form::FILLED, "Potvrzovací heslo musí být vyplněné !");
		}
		$form->addText('jmeno', 'Jméno *')
			->setAttribute('class', 'form-control')
			->setRequired("Vyplňte jméno!");
		$form->addText('prijmeni', 'Přijmení *')
			->setAttribute('class', 'form-control')
			->setRequired("Vyplňte přijmení!");
		if($id == null){
			$form->addSelect('role', 'Oprávnění *', array('0' => 'Administrator', '1' => 'Uživatel'))->setPrompt('Vyberte oprávnění')->setAttribute('class', 'form-control')->setRequired("Vyberte Oprávnění");
		}
		else{
			$form->addHidden('role');
		}
		$form->addHidden('id');
		$form->addSubmit('save', 'Uložit')->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = $this->uzivatelFormSubmitted;

		return $form;
	}

	public function uzivatelFormSubmitted($form){
		$values = $form->getValues();

		$values->password = Passwords::hash($values->password);
		unset($values->confirm_password);
		if($values->id == null){
			unset($values->id);
		}
		$uzivatel = new \App\Model\Entities\Uzivatel($values);
		if($this->user->isInRole(self::UZIVATEL)){
			$this->flashMessage('Na tuto akci nemáte oprávnění!', 'alert-warning');
			$this->redirect('Uzivatel:default');
		}
		$this->uzivatelHelper->save($uzivatel);
		$this->flashMessage("Uživatel byl uložen", 'alert-success');
		$this->redirect("Uzivatel:default");
	}

	public function handleSmazUzivatel($id){
		if(!$this->user->isInRole(self::ADMIN)){
			$this->flashMessage('Na tuto akci nemáte oprávnění!', 'alert-warning');
			$this->redirect('Uzivatel:default');
		}
		$this->uzivatelHelper->delete($id);
		$this->flashMessage('Uživatel byl smazán', 'alert-success');
		if($this->isAjax()){
			$this->redrawControl();
		}else{
			$this->redirect('Uzivatel:default');
		}
	}

	public function handleDeaktivovat($id){
		if(!$this->user->isInRole(self::ADMIN)){
			$this->flashMessage('Na tuto akci nemáte oprávnění!', 'alert-warning');
			$this->redirect('Uzivatel:default');
		}
		$user = $this->uzivatelHelper->getById($id);
		if($user->getActive() == 0){
			$user->setActive(1);
			$this->flashMessage('Uživatelský účet byl aktivován.', 'alert-success');
		}else{
			$user->setActive(0);
			$this->flashMessage('Uživatelský účet byl deaktivován.', 'alert-success');
		}
		$this->uzivatelHelper->save($user);
		if($this->isAjax()){
			$this->redrawControl();
		}else{
			$this->redirect('Uzivatel:default');
		}
	}
}