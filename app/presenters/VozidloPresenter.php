<?php

namespace App\Presenters;
use App\Model\Entities\Vozidlo;
use Nette\Application\UI\Form;
use Nette\Utils\Paginator;


class VozidloPresenter extends BasePresenter
{

	public function startup()
	{
		if (!$this->user->isLoggedIn()) {
			$backlink = $this->storeRequest();
			$this->forward('Sign:default', array('backlink' => $backlink));
		}
		parent::startup();
	}

	public function actionAdd($id = null){
		if($id) {
			$form = $this->getComponent('vozidloForm');
			$data = $this->vozidloHelper->getById($id);
			$data = $data->toArray();
			$data['stk'] = $data['stk']->format('d. m. Y');
			$form->setDefaults($data);
		}
	}

	public function renderAdd($id = null){
		$this->template->editace = false;
		if($id){
			$this->template->editace = true;
		}
	}

	public function renderDefault($page = 1, $q = null){
		$paginator = new Paginator();

		if($q){
			$data = $this->vozidloHelper->search($q);
			$count = count($data);
		}else{
			$count = $this->vozidloHelper->count();
		}

		$paginator->setItemCount($count);
		$paginator->setItemsPerPage(30);
		$paginator->setPage($page);

		if($q){
			$this->template->vozidla = $data;
		}else{
			$this->template->vozidla = $this->vozidloHelper->getAll('ASC', $paginator->getLength(), $paginator->getOffset());
		}
		$this->template->paginator = $paginator;
		$this->redrawControl();
	}

	public function createComponentVozidloForm(){
		$form = new Form();
		$form->addText('nazev', 'Název *')
				->setAttribute('class', 'form-control')
				->setRequired("Vyplňte název!");
		$form->addText('spz', 'SPZ *')
				->setAttribute('class', 'form-control')
				->setRequired("Vyplňte SPZ!");
		$form->addText('tachometr', 'Stav tachometru')
			->setAttribute('class', 'form-control');
		$form->addText('stk', 'STK')
			->setAttribute('class', 'form-control')
			->setAttribute('id', 'datum')
			->setAttribute('readonly', true);
		$form->addHidden('id');
		$form->addSubmit('save', 'Uložit')->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = $this->vozidloFormSubmitted;

		return $form;
	}

	public function vozidloFormSubmitted($form){
		$values = $form->getValues();
		$values->stk = new \DateTime($values->stk);

		if($values->id == null){
			unset($values->id);
		}
		$vozidlo = new Vozidlo($values);
		$this->vozidloHelper->save($vozidlo);
		$this->flashMessage("Vozidlo bylo uloženo.", 'alert-success');
		$this->redirect("Vozidlo:default");
	}

	public function handleSmazVozidlo($id){
		$this->vozidloHelper->delete($id);
		$this->flashMessage('Vozidlo bylo smazáno', 'alert-success');
		if($this->isAjax()){
			$this->redrawControl();
		}else{
			$this->redirect('Vozidlo:default');
		}
	}
}

