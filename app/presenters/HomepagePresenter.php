<?php

namespace App\Presenters;
use App\Model\Entities\Book;
use Nette\Application\UI\Form;
use Nette\Utils\Paginator;


use Nette;
use App\Model;


class HomepagePresenter extends BasePresenter
{

	public function renderDefault($page = 1){
		$paginator = new Paginator();

		$count = $this->bookHelper->count();

		$paginator->setItemCount($count);
		$paginator->setItemsPerPage(30);
		$paginator->setPage($page);


		$this->template->routes = $this->bookHelper->getAll('ASC', $paginator->getLength(), $paginator->getOffset());

		$this->template->paginator = $paginator;
		$this->redrawControl();
	}

	public function renderCalendar(){
		$this->template->items = $this->itemsForCalendar();
	}

	public function renderDetail($id){
		$this->template->route = $this->bookHelper->getById($id);
		$this->template->vydaje = $this->vydajeHelper->getByBookId($id);
	}

	public function actionAdd($id = null){
		if($id) {
			$form = $this->getComponent('bookForm');
			$data = $this->bookHelper->getById($id);
			$data = $data->toArray();
			$data['datum'] = $data['datum']->format('d. m. Y');
			$form->setDefaults($data);
		}
	}


	public function createComponentBookForm(){
		$vozidlo = $this->vozidloHelper->getAll();
		$arr = array();
		foreach($vozidlo as $v){
			$arr[$v->getId()] = $v->getNazev();
		}

		$form = new Form();
		$form->addText('datum', 'Datum')
				->setAttribute('class', 'form-control')
				->setAttribute('id', 'datum')
				->setAttribute('readonly', true);
		$form->addText('start', 'Odkud')
				->setAttribute('class', 'form-control')
				->setRequired("Vyplňte položku Odkud!");
		$form->addText('cil', 'Kam')
				->setAttribute('class', 'form-control')
				->setRequired("Vyplňte položku Kam!");
		$form->addSelect('typ', 'Typ', array(0 =>'Soukromá', 1 => 'Služební'))
			->setDefaultValue(1)
			->setAttribute('class', 'form-control')
			->setRequired("Vyplňte Typ!");
		$form->addSelect('id_vozidla', 'Vozidlo', $arr)
			->setPrompt('Vyberte Vozidlo')
			->setAttribute('class', 'form-control')
			->setRequired("Vyplňte Vozidlo!");
		$form->addText('vzdalenost', 'Vzdálenost')
				->setAttribute('class', 'form-control')
				->setRequired("Vyplňte vzdálenost!");
		$form->addHidden('id');
		if($this->user->isInRole(self::ADMIN)) {
			$form->addSelect('stav', 'Stav', array(0 => 'Zpracovává se', 1 => 'Proplaceno'))
				->setDefaultValue(0)
				->setAttribute('class', 'form-control')
				->setRequired("Vyplňte Stav!");
		}
		else{
			$form->addHidden('stav');
		}
		$form->addCheckbox('done', 'Dokončeno')
			->setDefaultValue(False)
			->setOption('description', 'Po vybrání nebude možno jízdu v budoucnu dále upravovat.');
		$form->addSubmit('save', 'Uložit')->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = $this->bookFormSubmitted;

		return $form;
	}

	public function bookFormSubmitted($form){
		$values = $form->getValues();
		$values->datum = new \DateTime($values->datum);
		if(!$this->user->isInRole(self::ADMIN)) {
			$values->stav = 0; // zpracovava se
		}

		if($values->id == null){
			unset($values->id);
		}

		$book = new Book($values);
		//die(var_dump($book->toArray()));
		$this->bookHelper->save($book);
		$this->flashMessage("Záznam byl uložen.", 'alert-success');
		$this->redirect("Homepage:default");
	}

	public function handleSmazBook($id){
		$this->bookHelper->delete($id);
		$this->flashMessage('Záznam byl smazán.', 'alert-success');
		if($this->isAjax()){
			$this->redrawControl();
		}else{
			$this->redirect('Homepage:default');
		}
	}

	public function createComponentPridatVydaj($name){
		$form = new Form();
		$form->addText('title', 'Popis')
			->setAttribute('class', 'form-control');
		$form->addText('price', 'Cena')
			->setAttribute('class', 'form-control')
			->addRule(Form::PATTERN, 'Zadejte prosím platnou cenu','\d*');
		$form->addSubmit('save', 'Uložit')
			->setAttribute('class', 'btn btn-primary');

		$form->onSuccess[] = $this->pridatVydajFormSubmitted;

		return $form;
	}

	public function pridatVydajFormSubmitted($form){
		$bookID = $this->getRequest()->getParameter('id');

		$values = $form->getValues();
		$values['book_id'] = $bookID;
		$vydaj = new Model\Entities\Vydaj($values);

		$this->vydajeHelper->save($vydaj);
		$this->flashMessage("Záznam byl uložen.", 'alert-success');
		$this->redirect("this");

	}

	public function itemsForCalendar(){
		$data = $this->bookHelper->getAll();
		$ret = '[';
		foreach($data as $item){
			$ret .= '{ title : \'Start: ' . $item->getStart() . '\nCíl: ' . $item->getCil() .'\n'.$item->getVzdalenost().' Km\',';
			$ret .= 'start : \'' . $item->getDatum()->format("Y-m-d") . '\',';
			if($item->getTyp() == 1){
				$ret .= 'color : \'#f0ad4e\', ';
			}
			$ret .= 'allDay : true },';
		}


		$ret .= ']';
		return $ret;
	}
}
